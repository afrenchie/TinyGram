package quentin.malinge.com;

import quentin.malinge.com.PMF;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.datanucleus.query.JDOCursorHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Nullable;
import javax.inject.Named;
import javax.persistence.EntityNotFoundException;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

@Api(name = "tmessageentityendpoint", namespace = @ApiNamespace(ownerDomain = "mitroglouglou.com", ownerName = "quentin.malinge", packagePath = "services"))
public class TMessageEntityEndPoint {
	
	/**
	 * This method lists all the entities inserted in datastore.
	 * It uses HTTP GET method and paging support.
	 *
	 * @return A CollectionResponse class containing the list of all entities
	 * persisted and a cursor to the next page.
	 */
	@SuppressWarnings({ "unchecked", "unused" })
	@ApiMethod(name = "listTMessageEntity")
	public CollectionResponse<TMessageEntity> listTMessageEntity(@Nullable @Named("cursor") String cursorString,
			@Nullable @Named("limit") Integer limit, @Nullable @Named("userid") Long userId) {
		System.out.println("listTMessageEntity CALLED");
		PersistenceManager mgr = null;
		Cursor cursor = null;
		List<TMessageEntity> execute = null;

		try {
			mgr = getPersistenceManager();
			Query query = mgr.newQuery(TMessageEntity.class);
			
			if (userId != null) {
				query.setFilter("userId == "+userId);
			}
			
			if (cursorString != null && cursorString != "") {
				cursor = Cursor.fromWebSafeString(cursorString);
				HashMap<String, Object> extensionMap = new HashMap<String, Object>();
				extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
				query.setExtensions(extensionMap);
			}

			if (limit != null) {
				query.setRange(0, limit);
			}

			execute = (List<TMessageEntity>) query.execute();
			cursor = JDOCursorHelper.getCursor(execute);
			if (cursor != null) {
				cursorString = cursor.toWebSafeString();
			}
			for (TMessageEntity obj : execute);
		} finally {
			mgr.close();
		}

		return CollectionResponse.<TMessageEntity>builder().setItems(execute).setNextPageToken(cursorString).build();
	}

	/**
	 * This method gets the entity having primary key id. It uses HTTP GET method.
	 *
	 * @param id the primary key of the java bean.
	 * @return The entity with primary key id.
	 */
	@ApiMethod(name = "gettmessageentity")
	public TMessageEntity getTMessageEntity(@Named("id") Long id) {
		PersistenceManager mgr = getPersistenceManager();
    	TMessageEntity tmessageentity = null;
    	try {
    		tmessageentity = mgr.getObjectById(TMessageEntity.class, id);
    	} finally {
    		mgr.close();
    	}
    	return tmessageentity;
	}

	/**
	 * This inserts a new entity into App Engine datastore. If the entity already
	 * exists in the datastore, an exception is thrown.
	 * It uses HTTP POST method.
	 *
	 * @param tmessageentity the entity to be inserted.
	 * @return The inserted entity.
	 */
	@ApiMethod(name = "insertTMessageEntity")
	public TMessageEntity insertTMessageEntity(TMessageEntity tmessageentity) {
		PersistenceManager mgr = getPersistenceManager();
		TMessageIndexEntity tmessageindexentity = null;
		List<Long> followers = new ArrayList();
		boolean contains = true;
		try {
			followers = mgr.getObjectById(TUserEntity.class, tmessageentity.getUserId()).getFollowers();
		} catch (javax.jdo.JDOObjectNotFoundException ex) {
			contains = false;
		} finally {
			mgr.close();
		}
		if(contains) {
			DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
			Key cleUser = KeyFactory.createKey("TUser", tmessageentity.getUserId());
			Entity messageentity = new Entity("TMessageEntity",cleUser);
			messageentity.setProperty("userId", tmessageentity.getUserId());
			messageentity.setProperty("body", tmessageentity.getBody());
			ds.put(messageentity);
			tmessageentity.setId(messageentity.getKey().getId());
			Entity indexentity = new Entity("TMessageIndexEntity",messageentity.getKey());
			indexentity.setProperty("followers", followers);
			indexentity.setProperty("hashtags", tmessageentity.getHashTags());
			ds.put(indexentity);
			//System.out.println(indexentity);
		}
		return tmessageentity;
	}

	/**
	 * This method is used for updating an existing entity. If the entity does not
	 * exist in the datastore, an exception is thrown.
	 * It uses HTTP PUT method.
	 *
	 * @param tmessageentity the entity to be updated.
	 * @return The updated entity.
	 */
	@ApiMethod(name = "updateTMessageEntity")
	public TMessageEntity updateTMessageEntity(TMessageEntity tmessageentity) {
		PersistenceManager mgr = getPersistenceManager();
		try {
			if (!containsTMessageEntity(tmessageentity)) {
				throw new EntityNotFoundException("Object does not exist");
			}
			mgr.makePersistent(tmessageentity);
		} finally {
			mgr.close();
		}
		return tmessageentity;
	}

	/**
	 * This method removes the entity with primary key id.
	 * It uses HTTP DELETE method.
	 *
	 * @param id the primary key of the entity to be deleted.
	 */
	@ApiMethod(name = "removeTMessageEntity")
	public void removeTMessageEntity(@Named("id") Long id) {
		PersistenceManager mgr = getPersistenceManager();
		try {
			TMessageEntity tmessageentity = mgr.getObjectById(TMessageEntity.class, id);
			mgr.deletePersistent(tmessageentity);
		} finally {
			mgr.close();
		}
	}

	private boolean containsTMessageEntity(TMessageEntity tmessageentity) {
		PersistenceManager mgr = getPersistenceManager();
		boolean contains = true;
		try {
			mgr.getObjectById(TMessageEntity.class, tmessageentity.getId());
		} catch (javax.jdo.JDOObjectNotFoundException ex) {
			contains = false;
		} finally {
			mgr.close();
		}
		return contains;
	}

	private static PersistenceManager getPersistenceManager() {
		return PMF.get().getPersistenceManager();
	}
}

