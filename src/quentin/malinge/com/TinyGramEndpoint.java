package quentin.malinge.com;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiMethod.HttpMethod;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.api.server.spi.config.Named;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.QueryResultList;

@Api(name = "tinygramapi", version="v1")
public class TinyGramEndpoint {
	
	@ApiMethod(
			path = "followersmessages",
	        httpMethod = HttpMethod.GET
	    )
	public CollectionResponse<TMessageEntity> getMessageFollower(@Nullable @Named("cursor") String cursorString,
			@Nullable @Named("limit") Integer limit, @Named("followerid") Long followerId) {
		List<TMessageEntity> execute = new ArrayList<TMessageEntity>();
		int limit_tm = 10;
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();

		Filter pf = new FilterPredicate("followers", FilterOperator.EQUAL, followerId);
		Query q = new Query("TMessageIndexEntity").setFilter(pf);
		q.setKeysOnly();

		PreparedQuery pq = ds.prepare(q);
		System.out.println(pq);
		if(limit != null) {
			if(limit > 0) {
				System.out.println("limite : "+limit);
				limit_tm = limit;
			}
		}
		FetchOptions fetchOptions = FetchOptions.Builder.withLimit(limit_tm);
		if (cursorString != null) {
			fetchOptions.startCursor(Cursor.fromWebSafeString(cursorString));
		}

	    QueryResultList<Entity> results = pq.asQueryResultList(fetchOptions);
		List<Key> pk = new ArrayList<Key>();
		Key k = null;
		for (Entity r : results) {
			k = r.getParent();
			if(k!=null) {
				pk.add(k);
			}
		}
		Map<Key, Entity> hm = new HashMap<Key, Entity>();
		
		if(!pk.isEmpty()) {
			hm = ds.get(pk);
			for (Entity ki : hm.values()) {
				TMessageEntity tm = new TMessageEntity((Long) ki.getProperty("id"), (Long) ki.getProperty("userId"), (String) ki.getProperty("body"));
				execute.add(tm);
			}
		}
		String nextCursorString = results.getCursor().toWebSafeString();
		return CollectionResponse.<TMessageEntity>builder().setItems(execute).setNextPageToken(nextCursorString).build();
    }
	
}
