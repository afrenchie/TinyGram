package quentin.malinge.com;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Random;

import javax.servlet.http.*;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

@SuppressWarnings("serial")
public class EntityGeneratorServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		long time;
		int TOTAL_USERS = 10;
		int TOTAL_MESSAGES = 3;
		int TOTAL_FOLLOWERS = 4;
		
		PrintWriter printer = resp.getWriter();
		Random r = new Random();
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		//String followers_string;
		long follower_index;
		long message_index = 1;
		resp.setContentType("text/html");
		
		//printer.println("<html><body>");
		//printer.println("<h2>Entity generation...</h2>");
		long t1 = System.currentTimeMillis();
		for (long i = (long) 1; i <= TOTAL_USERS; i++) {
			Entity user_e = new Entity("UserEntity");
			user_e.setProperty("first_name", "fname_" + i );
			user_e.setProperty("last_name", "lname_" + i );		
			
			ArrayList<Long> followers = new ArrayList<Long>();
			//followers_string = "Followers : [";
			for (long j = 1; j <= TOTAL_FOLLOWERS; j++) {
				follower_index = (long) r.nextInt(TOTAL_FOLLOWERS)+1;
				//System.out.println(follower_index);
				if(follower_index != i) {
					if(! followers.contains(i)) {
						followers.add(follower_index);
						//followers_string += " "+follower_index;
					}	
				}
				else {
					j--;
				}
			}
			user_e.setProperty("followers", followers);
			//printer.println("<b>Generating UserEntity n°"+i+"</b><br>");
			//printer.println("first_name "+user_e.getProperty("first_name"));
			//printer.println("last_name "+user_e.getProperty("last_name"));
			//followers_string += " ]</br>";
			datastore.put(user_e);
			
			//printer.println("<ul>");
			
			for (long j = 1; j <= TOTAL_MESSAGES; j++) {
				Entity message_e = new Entity("MessageEntity", message_index, user_e.getKey());
				message_e.setProperty("body", "Hello " + message_index);
				//printer.println("<li>MessageEntity n°"+message_index+"</li>");
				
				Entity message_index_e = new Entity("MessageIndexEntity",  message_index, message_e.getKey());
				message_index_e.setProperty("followers", followers);
				//printer.println("<li>MessageIndexEntity index n°"+message_index+"</li>");
				
				datastore.put(message_e);
				datastore.put(message_index_e);
				message_index++;
			}
			//printer.println("<li>followers "+followers_string+"</li>");
			//printer.println("</ul>");
		}
		time = System.currentTimeMillis() - t1;
		System.out.println(time);
		printer.println("Entity generation done in "+time);

	}
}

