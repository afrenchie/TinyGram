package quentin.malinge.com;

import quentin.malinge.com.PMF;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.config.ApiMethod.HttpMethod;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.datanucleus.query.JDOCursorHelper;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Nullable;
import javax.inject.Named;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

@Api(name = "tuserentityendpoint", namespace = @ApiNamespace(ownerDomain = "mitroglouglou.com", ownerName = "quentin.malinge", packagePath = "services"))
public class TUserEntityEndPoint {

	/**
	 * This method lists all the entities inserted in datastore.
	 * It uses HTTP GET method and paging support.
	 *
	 * @return A CollectionResponse class containing the list of all entities
	 * persisted and a cursor to the next page.
	 */
	@SuppressWarnings({ "unchecked", "unused" })
	@ApiMethod(name = "listTUserEntity")
	public CollectionResponse<TUserEntity> listTUserEntity(@Nullable @Named("cursor") String cursorString,
			@Nullable @Named("limit") Integer limit) {

		PersistenceManager mgr = null;
		Cursor cursor = null;
		List<TUserEntity> execute = null;

		try {
			mgr = getPersistenceManager();
			Query query = mgr.newQuery(TUserEntity.class);
			if (cursorString != null && cursorString != "") {
				cursor = Cursor.fromWebSafeString(cursorString);
				HashMap<String, Object> extensionMap = new HashMap<String, Object>();
				extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
				query.setExtensions(extensionMap);
			}

			if (limit != null) {
				query.setRange(0, limit);
			}

			execute = (List<TUserEntity>) query.execute();
			cursor = JDOCursorHelper.getCursor(execute);
			if (cursor != null) {
				cursorString = cursor.toWebSafeString();
			}
			for (TUserEntity obj : execute);
		} finally {
			mgr.close();
		}

		return CollectionResponse.<TUserEntity>builder().setItems(execute).setNextPageToken(cursorString).build();
	}

	/**
	 * This method gets the entity having primary key id. It uses HTTP GET method.
	 *
	 * @param id the primary key of the entity
	 * @return The entity with primary key id.
	 */
	@ApiMethod(name = "getTUserEntity")
	public TUserEntity getTUserEntity(@Named("id") Long id) {
		PersistenceManager mgr = getPersistenceManager();
		TUserEntity tuserentity = null;
		try {
			tuserentity = mgr.getObjectById(TUserEntity.class, id);
		} finally {
			mgr.close();
		}
		return tuserentity;
	}

	/**
	 * This inserts a new entity into App Engine datastore. If the entity already
	 * exists in the datastore, an exception is thrown.
	 * It uses HTTP POST method.
	 *
	 * @param tuserentity the entity to be inserted.
	 * @return The inserted entity.
	 */
	@ApiMethod(name = "insertTUserEntity")
	public TUserEntity insertTUserEntity(TUserEntity tuserentity) {
		PersistenceManager mgr = getPersistenceManager();
		try {
			if(tuserentity.getId() != null) {
				if (containsTUserEntity(tuserentity)) {
					throw new EntityExistsException("Object already exists");
				}
			}
			mgr.makePersistent(tuserentity);
		} finally {
			mgr.close();
		}
		return tuserentity;
	}

	/**
	 * This method is used for updating an existing entity. If the entity does not
	 * exist in the datastore, an exception is thrown.
	 * It uses HTTP PUT method.
	 *
	 * @param tuserentity the entity to be updated.
	 * @return The updated entity.
	 */
	@ApiMethod(name = "updateTUserEntity")
	public TUserEntity updateTUserEntity(TUserEntity tuserentity) {
		PersistenceManager mgr = getPersistenceManager();
		try {
			if (!containsTUserEntity(tuserentity)) {
				throw new EntityNotFoundException("Object does not exist");
			}
			mgr.makePersistent(tuserentity);
		} finally {
			mgr.close();
		}
		return tuserentity;
	}

	/**
	 * This method removes the entity with primary key id.
	 * It uses HTTP DELETE method.
	 *
	 * @param id the primary key of the entity to be deleted.
	 */
	@ApiMethod(name = "removeTUserEntity")
	public void removeTUserEntity(@Named("id") Long id) {
		PersistenceManager mgr = getPersistenceManager();
		try {
			TUserEntity tuserentity = mgr.getObjectById(TUserEntity.class, id);
			mgr.deletePersistent(tuserentity);
		} finally {
			mgr.close();
		}
	}

	
	@ApiMethod(
			path = "addfollower",
	        httpMethod = HttpMethod.POST
	    )
	public void addFollower(@Named("userid") Long userId, @Named("followerid") Long followerId) {
		PersistenceManager mgr = getPersistenceManager();
		try {
			TUserEntity tuserentity = mgr.getObjectById(TUserEntity.class, userId);
			if(userId != followerId) {
				tuserentity.addFollower(followerId);
			}
			mgr.makePersistent(tuserentity);
		} finally {
			mgr.close();
		}
	}
	
	private boolean containsTUserEntity(TUserEntity tuserentity) {
		PersistenceManager mgr = getPersistenceManager();
		boolean contains = true;
		try {
			mgr.getObjectById(TUserEntity.class, tuserentity.getId());
		} catch (javax.jdo.JDOObjectNotFoundException ex) {
			contains = false;
		} finally {
			mgr.close();
		}
		return contains;
	}

	private static PersistenceManager getPersistenceManager() {
		return PMF.get().getPersistenceManager();
	}

}
